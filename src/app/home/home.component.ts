import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  selectedFileContent: string = ''; // Define the property here
  constructor() { }

  ngOnInit(): void {
  }

  onFileSelected(content: string) {
    this.selectedFileContent = content;
  }

  // Perform the search operation based on the input value
  performSearch(searchTerm: string) {
    // Implement your search logic here, for example, filter the MD files based on the search term
    // Once you have the filtered result, you can handle displaying the filtered content accordingly
    console.log('Search Term:', searchTerm);
    // Implement your logic to filter and display content from MD files based on the search term
  }

}
