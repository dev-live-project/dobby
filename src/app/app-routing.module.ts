import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

// Import the 3 new components/sections of the webpage so that we can configure routing
import { AboutComponent } from './about/about.component';
import { HomeComponent } from './home/home.component';
import { ContactComponent } from './contact/contact.component';
import { NavigationComponent } from './navigation/navigation.component';
import { ContentDisplayComponent } from './content-display/content-display.component';

// Declare the paths that will be used when navigating between pages.
const routes: Routes = [
  { path: '', component: HomeComponent }, // http://websitedomain.com
  { path: 'about', component: AboutComponent }, // http://websitedomain.com/about
  { path: 'contact', component: ContactComponent }, // http://websitedomain.com/contact
  { path: 'nav', component: NavigationComponent },
  { path: 'file/:id', component: ContentDisplayComponent }, // Adjust this route as needed
];

@NgModule({
  imports: [
    CommonModule, 
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
