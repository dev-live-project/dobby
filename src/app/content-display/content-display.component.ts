import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-content-display',
  templateUrl: './content-display.component.html',
  styleUrls: ['./content-display.component.css'],
})
export class ContentDisplayComponent {
  @Input() content: string = '';
}
