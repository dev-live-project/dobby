import { Component, EventEmitter, Output } from '@angular/core';
import { MdFileService } from '../service/md-file.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css'],
})
export class NavigationComponent {
  @Output() fileSelected = new EventEmitter<string>();

  mdFiles = [
    'Languages/CaramelCheesecakeRecipe.md',
    'Languages/ChocolateCheesecakeRecipe.md',
    'Concepts/StrawberryCheesecakeRecipe.md'
    // ... other files and folders
  ]; // List of your MD files

  constructor(private mdFileService: MdFileService) {}

  onSelectFile(fileName: string) {
    this.mdFileService.getMdFileContent(fileName).subscribe((content) => {
      this.fileSelected.emit(content);
    });
  }

  // Method to get files within a specific folder
  getFilesInFolder(folderName: string): string[] {
    const filesInFolder = this.mdFiles.filter(file => file.startsWith(`${folderName}/`));
    return filesInFolder.map(file => file.split('/').pop() || ''); // Extract file names
  }

}
