import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class MdFileService {
  constructor(private http: HttpClient) {}

  getMdFileContent(fileName: string): Observable<string> {
    return this.http.get(`assets/posts/${fileName}`, { responseType: 'text' });
  }
}
